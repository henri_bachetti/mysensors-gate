
#include <Arduino.h>

#define LOG_DOMAIN LOG_SOLAR
#include "debug.h"
#include "solar.h"

#define VREF_LDR              5.0
#define PANEL_DIVIDER         0.048   // 1MΩ + 47KΩ
#define BATT_DIVIDER          0.248   // 1MΩ + 330KΩ
#define ANALOG_DELAY          100
#define SHUNT                 0.100   // shunt resistor on INA138 module
#define INA138_GAIN           19      // INA138 has a 100K RL resistor (gain=20)

// these values are OK for a GL5528 LDR
#define RREF                  100000.0
#define LUX_CALC_SCALAR       14500000
#define LUX_CALC_EXPONENT     -1.405
#define NSAMPLE               10

SolarPowerSupply::SolarPowerSupply()
{
  pinMode(CHARGE_ENABLE_PIN, OUTPUT);
}

void SolarPowerSupply::enableCharger(int state)
{
  digitalWrite(CHARGE_ENABLE_PIN, state);
}

#if OPEN_CLOSE_METHOD == USE_LDR
float SolarPowerSupply::getLuminosity(void)
{
  uint32_t adc = 0;

  analogReference(DEFAULT);
  analogRead(LDR_PIN);
  delay(5);
  for (int i = 0 ; i < NSAMPLE ; i++) {
    adc += analogRead(LDR_PIN);
    delay(ANALOG_DELAY);
  }
  adc /= NSAMPLE;
  float voltage = VREF_LDR - (adc * VREF_LDR / 1023);
  float current = (float)adc * VREF_LDR / 1023 / RREF;
  float resistance = voltage / current;
  log_print(F("LDR ADC: ")); log_println(adc);
  log_print(F("LDR V: ")); log_println(voltage);
  log_print(F("LDR I: ")); log_println(current * 1000);
  log_print(F("LDR R: ")); log_println(resistance);
  return (LUX_CALC_SCALAR * pow(resistance, LUX_CALC_EXPONENT));
}
#endif

float SolarPowerSupply::getPanelVoltage(void)
{
#if OPEN_CLOSE_METHOD == USE_PANEL
  // disable charger to mesaure voltage
  enableCharger(false);
#endif
  analogReference(INTERNAL);
  analogRead(SOLAR_PIN);
  delay(ANALOG_DELAY);
  unsigned int adc = analogRead(SOLAR_PIN);
  log_print(F("vpanel ADC: ")); log_println(adc);
  float voltage = adc * VREF / 1023 / PANEL_DIVIDER;
#if OPEN_CLOSE_METHOD == USE_PANEL
  enableCharger(true);
#endif
  return voltage;
}

#ifdef USE_INA138
float SolarPowerSupply::getPanelCurrent(void)
{
  analogReference(INTERNAL);
  analogRead(SOLAR_PIN);
  delay(ANALOG_DELAY);
  unsigned int adc = analogRead(SOLAR_I_PIN);
  log_print(F("ipanel ADC: ")); log_println(adc);
  float voltage = adc * VREF / 1023;
  float current = voltage / SHUNT / INA138_GAIN;
  return current;
}
#endif

struct batteryCapacity
{
  float voltage;
  int capacity;
};

const batteryCapacity remainingCapacity[] = {
  4.20,   100,
  4.15,   98,
  4.10,   96,
  4.00,   92,
  4.05,   90,
  3.96,   89,
  3.92,   85,
  3.89,   81,
  3.86,   77,
  3.83,   73,
  3.80,   69,
  3.77,   65,
  3.75,   62,
  3.72,   58,
  3.70,   55,
  3.66,   51,
  3.62,   47,
  3.58,   43,
  3.55,   40,
  3.51,   35,
  3.48,   32,
  3.44,   26,
  3.40,   24,
  3.37,   20,
  3.35,   17,
  3.27,   13,
  3.20,   9,
  3.1,    6,
  3.00,   3,
};

const int ncell = sizeof(remainingCapacity) / sizeof(struct batteryCapacity);

float SolarPowerSupply::getBatteryVoltage(bool waitDelay)
{
#ifdef USE_BATT_DIVIDER
  analogReference(INTERNAL);
  analogRead(BATT_PIN);
  if (waitDelay) {
    delay(ANALOG_DELAY);
  }
  unsigned int adc = analogRead(BATT_PIN);
//  log_print(F("Batt ADC: ")); log_println(adc);
  return adc * VREF / 1023 / BATT_DIVIDER;
#else
  return (1023 * VREF) / analogReadReference();
#endif
}

unsigned int SolarPowerSupply::getBatteryCapacity(float *vBatt)
{
  *vBatt = getBatteryVoltage(true);
  log_print(F("Batt VCC: ")); log_println(*vBatt, 3);
  for (int i = 0 ; i < ncell ; i++){
    log_print(i);
    log_print(F(" : "));
    log_print(remainingCapacity[i].voltage);
    log_print(F(" | "));
    log_println(remainingCapacity[i].capacity);
    if (*vBatt > remainingCapacity[i].voltage) {
      return remainingCapacity[i].capacity;
    }
  }
  return 0;
}
