
#ifndef _MG996R_H_
#define _MG996R_H_

#include <Servo.h>

#define UNKNOWN       -1
#define MAX_POS       180

class MG996R : public Servo
{
  private:
    int m_pwmPin;
    int m_vccPin;
    int m_state;

  public :
    MG996R(int pwmPin, int vccPin);
    void begin(void);
    int state(void) {return m_state;}
    void open(void (*callback)(void));
    void close(void (*callback)(void));
    void powerOff(void);
};

#endif
