
#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <Arduino.h>

#define LOG_MAIN        0x01
#define LOG_MOTOR       0x02
#define LOG_SOLAR       0x04
#define LOG_TIMEDATE    0x08

#define LOGS_ACTIVE     (LOG_MAIN | LOG_MOTOR | LOG_TIMEDATE)

#if (LOGS_ACTIVE & LOG_DOMAIN)
#define log_print       Serial.print
#define log_println     Serial.println
#define log_printf      Serial.printf
#else
#define log_print
#define log_println
#define log_printf
#endif

#endif
