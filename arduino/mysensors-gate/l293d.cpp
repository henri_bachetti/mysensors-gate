
#include <Arduino.h>

#define LOG_DOMAIN LOG_MOTOR
#include "debug.h"
#include "options.h"
#include "lowpower.h"
#include "l293d.h"

#define NSAMPLE   200

L293D::L293D(int en, int a, int b, int vcc1, int vcc2)
{
  pinMode(en, OUTPUT);
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(vcc1, OUTPUT);
  pinMode(vcc2, OUTPUT);
  pinE = en;
  pinA = a;
  pinB = b;
  pinVcc1 = vcc1;
  pinVcc2 = vcc2;
  m_state = UNKNOWN; // unknown state
}

void L293D::begin(void)
{
}

float L293D::getCurrent(void)
{
  unsigned long adc = 0;

  analogReference(INTERNAL);
  analogRead(IMOTOR_PIN);
  for (int sample = 0 ; sample < NSAMPLE ; sample++) {
    adc += analogRead(IMOTOR_PIN);
  }
  adc /= NSAMPLE;
//  log_print(F("imotor ADC: ")); log_println(adc);
  return adc * VREF / IMOTOR_SHUNT / 1023;
}

void L293D::set(int pwm, void (*callback)(void))
{
  unsigned long start = millis();
  if (pwm == 0) {
    digitalWrite(pinA, LOW);
    digitalWrite(pinB, LOW);
    digitalWrite(pinE, LOW);
    digitalWrite(pinVcc1, HIGH);
    digitalWrite(pinVcc2, HIGH);
  }
  else if (pwm > 0 && pwm <= 255) {
    digitalWrite(pinVcc1, LOW);
    digitalWrite(pinVcc2, LOW);
    delay(500);
    digitalWrite(pinE, LOW);
    digitalWrite(pinA, HIGH);
    digitalWrite(pinB, LOW);
    analogWrite(pinE, pwm);
    while (1) {
      callback();
      if (millis() - start > 1000) {
        float i = getCurrent();
        log_print(F("Motor I: ")); log_print((int)(i * 1000)); log_println("mA");
        if (i > MOTOR_CURRENT) {
          log_println("overcurrent, STOP");
          break;
        }
      }
      if (millis() - start > MOTOR_MAX_ONTIME) {
        log_println("overtime, STOP");
        break;
      }
    }
    m_state = HIGH;
  }
  else if (pwm < 0 && pwm >= -255) {
    digitalWrite(pinVcc1, LOW);
    digitalWrite(pinVcc2, LOW);
    delay(500);
    digitalWrite(pinE, LOW);
    digitalWrite(pinA, LOW);
    digitalWrite(pinB, HIGH);
    analogWrite(pinE, -pwm);
    while (1) {
      callback();
      if (millis() - start > 1000) {
        float i = getCurrent();
        log_print(F("Motor I: ")); log_print((int)(i * 1000)); log_println("mA");
        if (i > MOTOR_CURRENT) {
          log_println("overcurrent, STOP");
          break;
        }
        if (millis() - start > MOTOR_MAX_ONTIME) {
          log_println("overtime, STOP");
          break;
        }
      }
    }
    m_state = LOW;
  }
  if (pwm != 0) {
    log_print(F("time: "));  log_print((millis() - start) / 1000); log_println(F(" seconds"));
#ifdef USE_LIMIT_SWITCH
    m_state = digitalRead(LIMIT_SWITCH_PIN);
    log_print(F("limit switch: "));  log_println(m_state);
#endif
  }
}

int L293D::state(void)
{
  return m_state;
}
