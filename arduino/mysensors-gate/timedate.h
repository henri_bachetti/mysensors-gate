
#ifndef _TIMEDATE_H_
#define _TIMEDATE_H_

#include "options.h"
#include <time.h>

#if OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ
#include "RTClib.h"

#define SECS_PER_MIN  ((time_t)(60UL))
#define SECS_PER_HOUR ((time_t)(3600UL))
#define SECS_PER_DAY  ((time_t)(SECS_PER_HOUR * 24UL))
#define SECS_PER_MINUTES 60

enum week_t {Last, First, Second, Third, Fourth};
enum dow_t {Sun, Mon, Tue, Wed, Thu, Fri, Sat};
enum month_t {Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec};

struct ruleDST
{
  char timezone[6];
  int8_t week;
  int8_t wday;
  int8_t month;
  int8_t hour;
  int offset;
};

class GateScheduler
{
  private:
    RTC_DS3231 m_rtc;
    struct ruleDST dstStart, dstEnd;
    time_t calcDateDST(struct ruleDST rule, int year);
    int getTimeZoneOffset(time_t utc);
    time_t dstTime, stdTime;
    uint16_t yearDST;

  public:
    GateScheduler();
    void dst(const char* tzName, int8_t week, int8_t wday, int8_t month, int8_t hour, int tzOffset);
    void std(const char* tzName, int8_t week, int8_t wday, int8_t month, int8_t hour, int tzOffset);
    DateTime now(void)                {
      return m_rtc.now();
    }
    void adjust(const DateTime& dt)   {
      m_rtc.adjust(dt);
    }
    void adjust(unsigned long now);
    bool begin(void);
    bool isDay(void);
};

#endif
#endif
