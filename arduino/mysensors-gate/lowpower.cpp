
#include <Arduino.h>

#include "options.h"
#include "lowpower.h"

#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
int8_t sleep(const uint8_t interrupt1, const uint8_t mode1, const uint8_t interrupt2,
             const uint8_t mode2, const uint32_t sleepingMS = 0, const bool smartSleep = false);
#else
#include <LowPower.h>
#endif

void lowPowerBegin(void)
{
  pinMode(OPEN_PIN, INPUT_PULLUP);
  pinMode(CLOSE_PIN, INPUT_PULLUP);
}

void wakeUp(void)
{
}

int buttonPressed(void)
{
  int btn = 0;
  while (digitalRead(OPEN_PIN) == LOW) {
    if (btn != OPEN_PIN) {
      Serial.println("OPEN pressed");
      btn = OPEN_PIN;
    }
    if (digitalRead(CLOSE_PIN) == LOW) {
      Serial.println("2 buttons pressed");
      while (digitalRead(CLOSE_PIN) == LOW);
      while (digitalRead(OPEN_PIN) == LOW);
      return OPEN_AND_CLOSE;
    }
  }
  while (digitalRead(CLOSE_PIN) == LOW) {
    if (btn != CLOSE_PIN) {
      Serial.println("CLOSE pressed");
      btn = CLOSE_PIN;
    }
    if (digitalRead(OPEN_PIN) == LOW) {
      Serial.println("2 buttons pressed");
      while (digitalRead(CLOSE_PIN) == LOW);
      while (digitalRead(OPEN_PIN) == LOW);
      return OPEN_AND_CLOSE;
    }
  }
  return btn;
}

int lowPowerSleep(int seconds, int *btn)
{
  int sleeps = seconds / 4;
  int elapsed = 0;

  Serial.print("SLEEP "); Serial.println(seconds);
  delay(10);
  attachInterrupt(0, wakeUp, LOW);
  attachInterrupt(1, wakeUp, LOW);
  for (int i = 0 ; i < sleeps ; i++) {
#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
    sleep(digitalPinToInterrupt(OPEN_PIN), CHANGE, digitalPinToInterrupt(CLOSE_PIN), CHANGE, 4000);
#else
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
#endif
    int pressed = buttonPressed();
    if (pressed) {
      if (btn != 0) {
        *btn = pressed;
      }
      return elapsed;
    }
    elapsed += 4;
  }
  sleeps = seconds % 4;
  for (int i = 0 ; i < sleeps ; i++) {
#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
    sleep(digitalPinToInterrupt(OPEN_PIN), CHANGE, digitalPinToInterrupt(CLOSE_PIN), CHANGE, 1000);
#else
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
#endif
    int pressed = buttonPressed();
    if (pressed) {
      if (btn != 0) {
        *btn = pressed;
      }
      return elapsed;
    }
    elapsed += 1;
  }
  detachInterrupt(0);
  detachInterrupt(1);
  return elapsed;
}
