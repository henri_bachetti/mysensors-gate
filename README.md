# MYSENSORS GATE

The purpose of this page is to explain step by step the realization of an automated gate based on ARDUINO MINI, optionnaly connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

This purpose of this system is to be open in the morning and closed at night.

It can be used to control the gate of a chicken coop for exemple.

It can be powered with a LITHIUM-ION or LIPO battery and a solar pannel.

The system uses the following components :

 * an ARDUINO PRO MINI
 * an NRF24L01 (optional)
 * an RTC DS3231 (optional)
 * a temperature and humidity sensor : SHT31D (optionnal)
 * a DC motor or a servomotor
 * a solar pannel
 * an L293D chip (DC motor version)
 * a 5V MP1584 or LM2596 (adjustable) step-down converter (if solar pannel voltage is > 6V)
 * an SX1308 or MT3608 step-up converter (DC motor version)
 * an FP6293 step-up converter (servomotor version)
 * an INA138 module (optional)
 * 2 buttons (manual open and close)
 * some passive components
 * the board is powered by one or two 18650 batteries

### ELECTRONICS

The schema is made using KICAD.
The schematics of this version includes all the components.

### ARDUINO

The software is highly customizable (see options.h) to fit your needs :

 * DC motor or servomotor
 * open / close method (luminosity from LDR or solar pannel, sun rise and sun set)
 * DOMOTICZ server or not.
 * etc.

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/04/porte-motorisee-de-poulailler-1ere.html

![Screenshot](door-dc-motor.jpg)

