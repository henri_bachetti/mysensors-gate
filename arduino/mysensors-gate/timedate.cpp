
#include <Arduino.h>

#define LOG_DOMAIN LOG_TIMEDATE
#include "debug.h"
#include "timedate.h"

#if OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ

GateScheduler::GateScheduler()
{
  STD();
  DST();
  set_position(LATITUDE * ONE_DEGREE, LONGITUDE * ONE_DEGREE);
}

bool GateScheduler::begin(void)
{
  if (!m_rtc.begin()) {
    log_println(F("Couldn't find RTC"));
    return false;
  }
  if (m_rtc.lostPower()) {
    log_println(F("RTC lost power"));
    m_rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
}

void GateScheduler::std(const char* timezone, int8_t week, int8_t wday, int8_t month, int8_t hour, int offset) {
  strcpy(dstEnd.timezone, timezone);
  dstEnd.week = week;
  dstEnd.wday = wday;
  dstEnd.month = month;
  dstEnd.hour = hour;
  dstEnd.offset = offset;
}

void GateScheduler::dst(const char* timezone, int8_t week, int8_t wday, int8_t month, int8_t hour, int offset) {
  strcpy(dstStart.timezone, timezone);
  dstStart.week = week;
  dstStart.wday = wday;
  dstStart.month = month;
  dstStart.hour = hour;
  dstStart.offset = offset;
}

time_t GateScheduler::calcDateDST(struct ruleDST rule, int year)
{
  uint8_t month = rule.month;
  uint8_t week = rule.week;
  if (week == 0) {
    if (month++ > 11) {
      month = 0;
      year++;
    }
    week = 1;
  }
  struct tm tm;
  tm.tm_hour = rule.hour;
  tm.tm_min = 0;
  tm.tm_sec = 0;
  tm.tm_mday = 1;
  tm.tm_mon = month;
  tm.tm_year = year - 1900;
  time_t t = mktime(&tm);

  t += ((rule.wday - tm.tm_wday + 7) % 7 + (week - 1) * 7 ) * SECS_PER_DAY;
  if (rule.week == 0) t -= 7 * SECS_PER_DAY;
  return t;
}

int GateScheduler::getTimeZoneOffset(time_t utc)
{
  struct tm *current = gmtime(&utc);
  time_t utcSTD, utcDST;

  if (dstStart.timezone[0]) {
    dstTime = calcDateDST(dstStart, current->tm_year + 1900);
    utcDST = dstTime - (dstEnd.offset * SECS_PER_MINUTES);
    stdTime = calcDateDST(dstEnd, current->tm_year + 1900);
    utcSTD = stdTime - (dstStart.offset * SECS_PER_MINUTES);
    yearDST = current->tm_year + 1900;
    if ((utc > utcDST) && (utc <= utcSTD)) {
      return (dstStart.offset - dstEnd.offset) * SECS_PER_MINUTES + dstEnd.offset * SECS_PER_MINUTES;
    }
    return dstEnd.offset * SECS_PER_MINUTES;
  }
  return 0;
}

bool GateScheduler::isDay(void)
{
  time_t now;
  char buf[20];

  log_println("GateScheduler::isDay");
  DateTime utc = m_rtc.now();
  now = utc.unixtime();
  time_t sunRise = sun_rise(&now);
  time_t sunSet = sun_set(&now);
  sunSet += CLOSE_DELAY * 60;
#if (LOGS_ACTIVE & LOG_DOMAIN)
  log_print("now "); log_print(now);
  log_print(" "); log_print(utc.day()); log_print("/"); log_print(utc.month());
  log_print(" "); log_print(utc.hour()); log_print("h"); log_println(utc.minute());
  struct tm *utcSunRise = gmtime(&sunRise);
  log_print("sun rise "); log_print(sunRise);
  log_print(" "); log_print(utcSunRise->tm_mday); log_print("/"); log_print(utcSunRise->tm_mon + 1);
  log_print(" "); log_print(utcSunRise->tm_hour); log_print("h"); log_println(utcSunRise->tm_min);
  struct tm *utcSunSet = gmtime(&sunSet);
  log_print("sun set "); log_print(sunSet);
  log_print(" "); log_print(utcSunSet->tm_mday); log_print("/"); log_print(utcSunSet->tm_mon + 1);
  log_print(" "); log_print(utcSunSet->tm_hour); log_print("h"); log_println(utcSunSet->tm_min);
#endif
  return now >= sunRise && now <= sunSet ? true : false;
}

void GateScheduler::adjust(unsigned long now)
{
  // we receive local time from DOMOTICZ
  // convert it to UTC
  time_t offset = getTimeZoneOffset(now);
  log_print(F("UTC offset: ")); log_println(offset);
  m_rtc.adjust(DateTime(now-offset));
}

#endif
