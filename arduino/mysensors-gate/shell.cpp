
#include <Arduino.h>

#define LOG_DOMAIN LOG_MAIN
#include "debug.h"
#include "timedate.h"
#include "lowpower.h"
#include "shell.h"

#define CMD_MAX       30

#if OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ

extern GateScheduler gateScheduler;
extern unsigned long manualWakeupTime;

void waitForShellCommand(void)
{
  while (manualWakeupTime != 0) {
    if (buttonPressed()) {
      log_println(F("stop manual wakeup"));
      manualWakeupTime = 0;
    }
    if (millis() - manualWakeupTime >= MANUAL_WAKEUP * 1000) {
      log_println(F("reset manual wakeup"));
      manualWakeupTime = 0;
    }
    if (Serial.available()) {
      static const char *monthNames[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
      static char buf[CMD_MAX];
      static int i;
      int c = Serial.read();
      if (c != -1) {
        if (c == '\n') {
          if (i == 0) {
            return;
          }
          if (!strncmp(buf, "date ", 5)) {
            static char t[10];
            DateTime now = gateScheduler.now();
            sprintf(t, "%d:%d:%d", now.hour(), now.minute(), now.second());
            log_print(buf + 5); log_print(" "); log_println(t);
            gateScheduler.adjust(DateTime(buf + 5, t));
            manualWakeupTime = 0;
            break;
          }
          else if (!strncmp(buf, "time ", 5)) {
            static char d[10];
            DateTime now = gateScheduler.now();
            sprintf(d, "%s %2d %d", monthNames[now.month() - 1], now.day(), now.year());
            log_print(d); log_print(" "); log_println(buf + 5);
            gateScheduler.adjust(DateTime(d, buf + 5));
            manualWakeupTime = 0;
            break;
          }
          else if (!strncmp(buf, "reset", 5)) {
            log_print(__DATE__); log_print(" "); log_println(__TIME__);
            gateScheduler.adjust(DateTime(__DATE__, __TIME__));
            manualWakeupTime = 0;
            break;
          }
          else {
            // command not supported
            log_println("WHAT'S UP DOC ?");
            manualWakeupTime = 0;
            break;
          }
          buf[i] = 0;
          i = 0;
          return;
        }
        if (c != '\r') {
          if (i < CMD_MAX - 1) {
            buf[i++] = c;
            buf[i] = '\0';
          }
        }
      }
    }
  }
}

#endif
