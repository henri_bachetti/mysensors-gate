
#define SERVO_MOTOR       1
#define DC_MOTOR          2
// define MOTOR as SERVO_MOTOR or DC_MOTOR according to the hardware
#define MOTOR             DC_MOTOR

#define NONE              0
#define HDC2080           1
#define SHT31D            2
#define HTU21D            3
// define SENSOR according to the temperature/humidité module used
#define SENSOR            SHT31D

// DOMOTICZ time request period in seconds
// this parameter is used only if OPEN_CLOSE_METHOD is USE_DOMOTICZ
#define TIME_REQUEST_PERIOD   2592000   // 30 days

// we receive local time from DOMOTICZ
// we need to convert it to UTC with timezone
// this parameter is used only if OPEN_CLOSE_METHOD is USE_DOMOTICZ
// Europe/Paris : last sunday of october at 3:00 : 1H offset
#define STD() std("CET", Last, Sun, Oct, 3, 60);
// Europe/Paris : last sunday of march at 2:00 : 2H offset
#define DST() dst("CEST", Last, Sun, Mar, 2, 120);

// open / close method
#define USE_PANEL         1
#define USE_LDR           2
#define USE_RTC           3
#define USE_DOMOTICZ      4
#define OPEN_CLOSE_METHOD USE_DOMOTICZ

// close delay in minutes
#define CLOSE_DELAY       30

// if the solar panel is used to measure luminosity
// this parameter is used only if OPEN_CLOSE_METHOD is USE_PANEL
#define SOLAR_DAY_LEVEL   10
#define SOLAR_NIGHT_LEVEL 4

// if an LDR is used to measure luminosity
// this parameter is used only if OPEN_CLOSE_METHOD is USE_LDR
#define LDR_DAY_LEVEL     5
#define LDR_NIGHT_LEVEL   1

// if RTC or DOMOTICZ is used to calculate ephemeris
#define LATITUDE          47.25
#define LONGITUDE         6.033

// sleep time in seconds
#define SLEEP_TIME        300

// manual mode timeout in seconds
// if the door is open or closed manually
// it will be closed or open after this timeout according to OPEN_CLOSE_METHOD
#define MANUAL_TIMEOUT    300

// manual wakeup in seconds
// this time allows the user to change the date or time using the serial line
// this parameter is used only if OPEN_CLOSE_METHOD is USE_RTC
#define MANUAL_WAKEUP     30

// internal ADC reference voltage
// must be adjusted by comparison with the value displayed by a multimeter
// - battery voltage displayed on the terminal
// - pannel voltage displayed on the terminal
#define VREF              1.081
// L293D shunt
#define IMOTOR_SHUNT      1.5

// DC motor parameters for JGA25-370 60RPM https://fr.aliexpress.com/item/33011814483.html?spm=a2g0s.9042311.0.0.27426c37NBEivs
// L293D PWM : speed limitation
#define PWM               120
// DC motor max ON time
#define MOTOR_MAX_ONTIME  25000
// DC motor max current
#define MOTOR_CURRENT     0.120

// limit switch is used (recommended in a servomotor version)
//#define USE_LIMIT_SWITCH

// use the servomotor potentiometer voltage (servomotor version only)
// needs a servomoteor with analog control
// it is possible to modifiy an MG996R to have this option
// https://riton-duino.blogspot.com/2020/07/porte-motorisee-de-poulailler-le-bilan.html : chapter 8.2
//#define USE_SERVO_POT

// INA138 module (pannel current monitor)
// normally not used
#define USE_INA138

// minimum battery voltage : 3V at least
#define MIN_BATT_VOLTAGE  3.0

// use a resistor diviser to measure battery voltage
// if this line is commented, ARDUINO is powered directly by the battery
#define USE_BATT_DIVIDER

// digital pins (do not modify except if the hardware is modified)
#define OPEN_PIN          2 // open button
#define CLOSE_PIN         3 // close button
#define VCC1_PIN          6
#define VCC2_PIN          6
#define SERVO_PIN         9 // shared with L293D_EN_PIN
#define L293D_EN_PIN      9
#define L293D_1_PIN       4
#define L293D_2_PIN       10
#define LIMIT_SWITCH_PIN  A3
#define CHARGE_ENABLE_PIN 5

// analog pins (do not modify except if the hardware is modified)
#define LDR_PIN           A2
#define SOLAR_PIN         A0
#define SOLAR_I_PIN       A7
#define BATT_PIN          A1
#define IMOTOR_PIN        A6

#define OPEN_AND_CLOSE    1 // both buttons

#if defined USE_SERVO_POT && defined USE_LIMIT_SWITCH
#error USE_SERVO_POT and USE_LIMIT_SWITCH cannot be defined together
#endif
