
#ifndef _TEMP_H_
#define _TEMP_H_

extern unsigned long tempPeriod;

bool tempSensorBegin(void);
void getTemperatureHumidity(float *temperature, float *humidity);

#endif
