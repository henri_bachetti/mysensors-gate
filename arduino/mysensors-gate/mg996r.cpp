
#include <Arduino.h>

#define LOG_DOMAIN LOG_MOTOR
#include "debug.h"
#include "options.h"
#include "mg996r.h"

MG996R::MG996R(int pwmPin, int vccPin) :
  m_pwmPin(pwmPin),
  m_vccPin(vccPin),
  m_state(UNKNOWN)
{
}

void MG996R::begin(void)
{
  pinMode(m_vccPin, OUTPUT);
#ifdef USE_SERVO_POT
  analogReference(DEFAULT);
  int pot = analogRead(LIMIT_SWITCH_PIN);
  log_print(F("LIMIT SWITCH : ")); log_println(pot);
  m_state = pot > 400 ? HIGH : LOW;
#elif defined USE_LIMIT_SWITCH
  pinMode(LIMIT_SWITCH_PIN, INPUT);
  m_state = digitalRead(LIMIT_SWITCH_PIN);
#else
  m_state = LOW;
#endif
}

void MG996R::open(void (*callback)(void))
{
  log_print(F("power on ")); log_println(m_vccPin);
  for (int retry = 0 ; retry < 3 ; retry++) {
    digitalWrite(m_vccPin, LOW);
    delay(500);
    log_println(F("pos: 180"));
    attach(m_pwmPin);
    for (int pos = 0 ; pos < MAX_POS ; pos++) {
      write(pos);
      callback();
      delay(20);
    }
#ifdef USE_SERVO_POT
    analogReference(DEFAULT);
    int pot = analogRead(LIMIT_SWITCH_PIN);
    log_print(F("LIMIT SWITCH : ")); log_println(pot);
    if (pot > 400) {
      m_state = HIGH;
      break;
    }
    else {
      close(callback);
    }
#elif defined USE_LIMIT_SWITCH
    m_state = digitalRead(LIMIT_SWITCH_PIN);
    log_print(F("LIMIT SWITCH : ")); log_println(m_state);
    if (m_state == HIGH) {
      break;
    }
    else {
      close(callback);
    }
#else
    m_state = HIGH;
    break;
#endif
  }
}

void MG996R::close(void (*callback)(void))
{
  log_print(F("power on ")); log_println(m_vccPin);
  digitalWrite(m_vccPin, LOW);
  delay(500);
  log_println(F("pos: 0"));
  attach(m_pwmPin);
  for (int pos = MAX_POS ; pos >= 0 ; pos--) {
    write(pos);
    callback();
    delay(20);
  }
#ifdef USE_SERVO_POT
  analogReference(DEFAULT);
  int pot = analogRead(LIMIT_SWITCH_PIN);
  log_print(F("LIMIT SWITCH : ")); log_println(pot);
  if (pot < 400) {
    m_state = LOW;
  }
#elif defined USE_LIMIT_SWITCH
  m_state = digitalRead(LIMIT_SWITCH_PIN);
  log_print(F("LIMIT SWITCH : ")); log_println(m_state);
#else
  m_state = LOW;
#endif
}

void MG996R::powerOff(void)
{
  log_println(F("power off"));
  detach();
  digitalWrite(m_vccPin, HIGH);
}
