
#ifndef L293D_H
#define L293D_H

#include "Arduino.h"

#define UNKNOWN       -1

class L293D
{
  private:
    int pinE;
    int pinA;
    int pinB;
    int pinVcc1;
    int pinVcc2;
    int m_state;
    float getCurrent(void);
  public:
    L293D(int en, int a, int b, int vcc1, int vcc2);
    void begin(void);
    void set(int pwm, void (*callback)(void));
    int state(void);
};

#endif
