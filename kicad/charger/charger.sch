EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-misc-modules
LIBS:my-power-supplies
LIBS:my-batteries
LIBS:my-microcontrollers
LIBS:my-ICs
LIBS:my-transistors
LIBS:my-power
LIBS:charger-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TP4056-MODULE U2
U 1 1 5E9380EE
P 6250 3450
F 0 "U2" H 6150 3350 60  0000 C CNN
F 1 "TP4056-MODULE" H 6100 3450 60  0000 C CNN
F 2 "" H 6250 3450 60  0000 C CNN
F 3 "" H 6250 3450 60  0000 C CNN
	1    6250 3450
	1    0    0    -1  
$EndComp
$Comp
L Battery BT1
U 1 1 5E93813E
P 7050 3550
F 0 "BT1" V 7150 3650 50  0000 L CNN
F 1 "3.7V 2500mAH" V 6850 3300 50  0000 L CNN
F 2 "" V 7050 3590 50  0000 C CNN
F 3 "" V 7050 3590 50  0000 C CNN
	1    7050 3550
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 5E938240
P 3800 3450
F 0 "P1" H 3800 3600 50  0000 C CNN
F 1 "SOLAR-PANNEL" V 3900 3450 50  0000 C CNN
F 2 "" H 3800 3450 50  0000 C CNN
F 3 "" H 3800 3450 50  0000 C CNN
	1    3800 3450
	-1   0    0    1   
$EndComp
$Comp
L LM2596-MODULE U1
U 1 1 5E9382CA
P 4750 3450
F 0 "U1" H 4750 3700 60  0000 C CNN
F 1 "MP1584" H 4550 3200 60  0000 L CNN
F 2 "" H 4750 3450 60  0000 C CNN
F 3 "" H 4750 3450 60  0000 C CNN
	1    4750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3550 6800 3550
Wire Wire Line
	6800 3350 7300 3350
Wire Wire Line
	7300 3350 7300 3550
Wire Wire Line
	7300 3550 7200 3550
Wire Wire Line
	4100 3400 4000 3400
Wire Wire Line
	4100 3350 4200 3350
Wire Wire Line
	4200 3550 4100 3550
Wire Wire Line
	4100 3550 4100 3500
Wire Wire Line
	4100 3500 4000 3500
Wire Wire Line
	5500 3200 5400 3200
Wire Wire Line
	5400 3200 5400 3350
Wire Wire Line
	5400 3350 5300 3350
Wire Wire Line
	5300 3550 5400 3550
Wire Wire Line
	5400 3550 5400 3700
Wire Wire Line
	5400 3700 5500 3700
$Comp
L CONN_01X02 P2
U 1 1 5EA01D86
P 7800 3450
F 0 "P2" H 7800 3600 50  0000 C CNN
F 1 "3.7V" V 7900 3450 50  0000 C CNN
F 2 "" H 7800 3450 50  0000 C CNN
F 3 "" H 7800 3450 50  0000 C CNN
	1    7800 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 3400 7500 3400
Wire Wire Line
	7500 3400 7500 3200
Wire Wire Line
	7500 3200 6800 3200
Wire Wire Line
	6800 3700 7500 3700
Wire Wire Line
	7500 3700 7500 3500
Wire Wire Line
	7500 3500 7600 3500
Wire Wire Line
	4100 3350 4100 3400
$EndSCHEMATC
