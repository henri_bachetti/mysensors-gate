EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L my_mcu:arduino_mini U2
U 1 1 5E9F6264
P 6350 3550
F 0 "U2" H 6350 4100 70  0000 C CNN
F 1 "arduino_mini" H 6350 3850 70  0000 C CNN
F 2 "myMicroControllers:Arduino-mini" H 6350 3100 60  0001 C CNN
F 3 "" H 6650 3050 60  0000 C CNN
	1    6350 3550
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5E9F62BB
P 5250 2700
F 0 "#PWR01" H 5250 2550 50  0001 C CNN
F 1 "+5V" H 5250 2840 50  0000 C CNN
F 2 "" H 5250 2700 50  0000 C CNN
F 3 "" H 5250 2700 50  0000 C CNN
	1    5250 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5E9F630F
P 5100 3050
F 0 "#PWR02" H 5100 2800 50  0001 C CNN
F 1 "GND" H 5100 2900 50  0000 C CNN
F 2 "" H 5100 3050 50  0000 C CNN
F 3 "" H 5100 3050 50  0000 C CNN
	1    5100 3050
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:C C1
U 1 1 5E9F74D2
P 4850 2950
F 0 "C1" V 4900 3000 50  0000 L CNN
F 1 "100nF" V 4900 2650 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 4888 2800 50  0001 C CNN
F 3 "" H 4850 2950 50  0000 C CNN
	1    4850 2950
	0    1    1    0   
$EndComp
$Comp
L mysensors-gate-rescue:R R3
U 1 1 5E9F81A5
P 8800 5650
F 0 "R3" V 8880 5650 50  0000 C CNN
F 1 "1M" V 8800 5650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 8730 5650 50  0001 C CNN
F 3 "" H 8800 5650 50  0000 C CNN
	1    8800 5650
	-1   0    0    1   
$EndComp
$Comp
L mysensors-gate-rescue:R R2
U 1 1 5E9F8317
P 8800 6150
F 0 "R2" V 8880 6150 50  0000 C CNN
F 1 "47K" V 8800 6150 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 8730 6150 50  0001 C CNN
F 3 "" H 8800 6150 50  0000 C CNN
	1    8800 6150
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E9F844B
P 8800 6400
F 0 "#PWR03" H 8800 6150 50  0001 C CNN
F 1 "GND" H 8800 6250 50  0000 C CNN
F 2 "" H 8800 6400 50  0000 C CNN
F 3 "" H 8800 6400 50  0000 C CNN
	1    8800 6400
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:R R10
U 1 1 5EA670D5
P 4300 4950
F 0 "R10" V 4380 4950 50  0000 C CNN
F 1 "100K" V 4300 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4230 4950 50  0001 C CNN
F 3 "" H 4300 4950 50  0000 C CNN
	1    4300 4950
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5EA672D9
P 4300 4100
F 0 "#PWR04" H 4300 3950 50  0001 C CNN
F 1 "+5V" H 4300 4240 50  0000 C CNN
F 2 "" H 4300 4100 50  0000 C CNN
F 3 "" H 4300 4100 50  0000 C CNN
	1    4300 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5EA673A8
P 4300 5200
F 0 "#PWR05" H 4300 4950 50  0001 C CNN
F 1 "GND" H 4300 5050 50  0000 C CNN
F 2 "" H 4300 5200 50  0000 C CNN
F 3 "" H 4300 5200 50  0000 C CNN
	1    4300 5200
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:SW_PUSH SW1
U 1 1 5EA68DFB
P 10150 3250
F 0 "SW1" H 10300 3360 50  0000 C CNN
F 1 "OPEN" H 10150 3170 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 10150 3250 50  0001 C CNN
F 3 "" H 10150 3250 50  0000 C CNN
	1    10150 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5EA68F64
P 10550 3700
F 0 "#PWR06" H 10550 3450 50  0001 C CNN
F 1 "GND" H 10550 3550 50  0000 C CNN
F 2 "" H 10550 3700 50  0000 C CNN
F 3 "" H 10550 3700 50  0000 C CNN
	1    10550 3700
	-1   0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:SW_PUSH SW2
U 1 1 5EA68EFB
P 10150 3550
F 0 "SW2" H 10300 3660 50  0000 C CNN
F 1 "CLOSE" H 10150 3470 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 10150 3550 50  0001 C CNN
F 3 "" H 10150 3550 50  0000 C CNN
	1    10150 3550
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:R R9
U 1 1 5EA6AD60
P 8400 5450
F 0 "R9" V 8480 5450 50  0000 C CNN
F 1 "1M" V 8400 5450 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 8330 5450 50  0001 C CNN
F 3 "" H 8400 5450 50  0000 C CNN
	1    8400 5450
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:R R1
U 1 1 5EA6B3D0
P 8400 5950
F 0 "R1" V 8480 5950 50  0000 C CNN
F 1 "330K" V 8400 5950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 8330 5950 50  0001 C CNN
F 3 "" H 8400 5950 50  0000 C CNN
	1    8400 5950
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5EA6B536
P 8400 6200
F 0 "#PWR07" H 8400 5950 50  0001 C CNN
F 1 "GND" H 8400 6050 50  0000 C CNN
F 2 "" H 8400 6200 50  0000 C CNN
F 3 "" H 8400 6200 50  0000 C CNN
	1    8400 6200
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:CONN_01X02 P5
U 1 1 5EA72130
P 3900 4550
F 0 "P5" H 3900 4700 50  0000 C CNN
F 1 "LDR" V 4000 4550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 3900 4550 50  0001 C CNN
F 3 "" H 3900 4550 50  0000 C CNN
	1    3900 4550
	-1   0    0    1   
$EndComp
$Comp
L my_misc_modules:DS3231 U8
U 1 1 5EA734AD
P 7350 5850
F 0 "U8" H 7400 5700 60  0000 C CNN
F 1 "DS3231" H 7550 6200 60  0000 C CNN
F 2 "myModules:ds3231-module-horz" H 7850 5250 60  0001 C CNN
F 3 "" H 7850 5250 60  0000 C CNN
	1    7350 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR08
U 1 1 5EA7385F
P 6350 5450
F 0 "#PWR08" H 6350 5300 50  0001 C CNN
F 1 "+5V" H 6350 5590 50  0000 C CNN
F 2 "" H 6350 5450 50  0000 C CNN
F 3 "" H 6350 5450 50  0000 C CNN
	1    6350 5450
	1    0    0    -1  
$EndComp
Text Notes 6900 5350 0    60   ~ 0
DS3231 is optional
Text Notes 3500 4850 0    60   ~ 0
LDR is optional
$Comp
L my_misc_modules:NRF24L01 U3
U 1 1 5EA74C49
P 7350 7950
F 0 "U3" H 7200 8200 60  0000 C CNN
F 1 "NRF24L01" H 7600 7500 60  0000 C CNN
F 2 "myModules:NRF24L01-PA-LNA" H 7350 7550 60  0001 C CNN
F 3 "" H 7350 7550 60  0000 C CNN
	1    7350 7950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5EA7508C
P 7350 8600
F 0 "#PWR09" H 7350 8350 50  0001 C CNN
F 1 "GND" H 7350 8450 50  0000 C CNN
F 2 "" H 7350 8600 50  0000 C CNN
F 3 "" H 7350 8600 50  0000 C CNN
	1    7350 8600
	1    0    0    -1  
$EndComp
$Comp
L my-regulators:HT7533-1 U1
U 1 1 5EA7587B
P 6100 7250
F 0 "U1" H 6250 7200 50  0000 C CNN
F 1 "HT7533-1" H 6100 7450 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 6100 7350 50  0001 C CIN
F 3 "" H 6100 7250 50  0000 C CNN
	1    6100 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5EA75A5E
P 6100 8150
F 0 "#PWR010" H 6100 7900 50  0001 C CNN
F 1 "GND" H 6100 8000 50  0000 C CNN
F 2 "" H 6100 8150 50  0000 C CNN
F 3 "" H 6100 8150 50  0000 C CNN
	1    6100 8150
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:CP C2
U 1 1 5EA7605D
P 5600 7450
F 0 "C2" H 5625 7550 50  0000 L CNN
F 1 "10µF" H 5625 7350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 5638 7300 50  0001 C CNN
F 3 "" H 5600 7450 50  0000 C CNN
	1    5600 7450
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:CP C3
U 1 1 5EA7626B
P 6600 7450
F 0 "C3" H 6625 7550 50  0000 L CNN
F 1 "10µF" H 6400 7350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 6638 7300 50  0001 C CNN
F 3 "" H 6600 7450 50  0000 C CNN
	1    6600 7450
	1    0    0    -1  
$EndComp
Text Notes 5450 8550 0    60   ~ 0
NRF24L01 and HT7533-1 are optional
Text GLabel 9450 4350 1    60   Input ~ 0
L293D-EN-OR-PWM
Text GLabel 9750 4350 1    60   Input ~ 0
L293D-1A
Text GLabel 9900 4350 1    60   Input ~ 0
L293D-2A
Text GLabel 10050 4350 1    60   Input ~ 0
POWER-ON
$Comp
L power:GND #PWR011
U 1 1 5EA82677
P 10250 5700
F 0 "#PWR011" H 10250 5450 50  0001 C CNN
F 1 "GND" H 10250 5550 50  0000 C CNN
F 2 "" H 10250 5700 50  0000 C CNN
F 3 "" H 10250 5700 50  0000 C CNN
	1    10250 5700
	1    0    0    -1  
$EndComp
Text GLabel 10200 4450 1    60   Input ~ 0
VBATT
$Comp
L power:+5V #PWR012
U 1 1 5EA82693
P 10400 4500
F 0 "#PWR012" H 10400 4350 50  0001 C CNN
F 1 "+5V" H 10400 4640 50  0000 C CNN
F 2 "" H 10400 4500 50  0000 C CNN
F 3 "" H 10400 4500 50  0000 C CNN
	1    10400 4500
	1    0    0    -1  
$EndComp
Text GLabel 9300 4350 1    60   Input ~ 0
VPANEL
Text GLabel 9150 4350 1    60   Input ~ 0
IPANEL
$Comp
L power:GND #PWR013
U 1 1 5EA8418D
P 6250 6100
F 0 "#PWR013" H 6250 5850 50  0001 C CNN
F 1 "GND" H 6250 5950 50  0000 C CNN
F 2 "" H 6250 6100 50  0000 C CNN
F 3 "" H 6250 6100 50  0000 C CNN
	1    6250 6100
	1    0    0    -1  
$EndComp
Text GLabel 9000 4350 1    60   Input ~ 0
CHARGE-ENABLE
$Comp
L power:+5V #PWR014
U 1 1 5EAACBCA
P 5600 7100
F 0 "#PWR014" H 5600 6950 50  0001 C CNN
F 1 "+5V" H 5600 7240 50  0000 C CNN
F 2 "" H 5600 7100 50  0000 C CNN
F 3 "" H 5600 7100 50  0000 C CNN
	1    5600 7100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR015
U 1 1 5EAAD782
P 6600 7100
F 0 "#PWR015" H 6600 6950 50  0001 C CNN
F 1 "+3.3V" H 6600 7240 50  0000 C CNN
F 2 "" H 6600 7100 50  0000 C CNN
F 3 "" H 6600 7100 50  0000 C CNN
	1    6600 7100
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:CONN_01X11 P1
U 1 1 5EAB63A9
P 10700 5100
F 0 "P1" H 10700 5700 50  0000 C CNN
F 1 "POWER-BOARD" V 10800 5100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x11" H 10700 5100 50  0001 C CNN
F 3 "" H 10700 5100 50  0000 C CNN
	1    10700 5100
	1    0    0    1   
$EndComp
Text GLabel 9600 4350 1    60   Input ~ 0
IMOTOR
$Comp
L my_misc_modules:SHT31D U4
U 1 1 5EAC230E
P 5450 5750
F 0 "U4" H 5050 5450 60  0000 C CNN
F 1 "SHT31D" H 5550 5450 60  0000 C CNN
F 2 "myModules:SHT31D" H 5950 5150 60  0001 C CNN
F 3 "" H 5950 5150 60  0000 C CNN
	1    5450 5750
	-1   0    0    1   
$EndComp
$Comp
L mysensors-gate-rescue:CP C4
U 1 1 5EAEF843
P 4450 4450
F 0 "C4" H 4475 4550 50  0000 L CNN
F 1 "10µF" H 4300 4350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 4488 4300 50  0001 C CNN
F 3 "" H 4450 4450 50  0000 C CNN
	1    4450 4450
	1    0    0    -1  
$EndComp
$Comp
L my_ics:MCP23008 U5
U 1 1 5EF314B8
P 9250 7500
F 0 "U5" H 9250 7750 50  0000 C CNN
F 1 "MCP23008" H 9300 7600 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_LongPads" H 9250 7600 60  0001 C CNN
F 3 "" H 9250 7600 60  0000 C CNN
	1    9250 7500
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR016
U 1 1 5EF317E7
P 9150 6400
F 0 "#PWR016" H 9150 6250 50  0001 C CNN
F 1 "+5V" H 9150 6540 50  0000 C CNN
F 2 "" H 9150 6400 50  0000 C CNN
F 3 "" H 9150 6400 50  0000 C CNN
	1    9150 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5EF31B64
P 8600 7700
F 0 "#PWR017" H 8600 7450 50  0001 C CNN
F 1 "GND" H 8600 7550 50  0000 C CNN
F 2 "" H 8600 7700 50  0000 C CNN
F 3 "" H 8600 7700 50  0000 C CNN
	1    8600 7700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR018
U 1 1 5EF33F84
P 10400 6550
F 0 "#PWR018" H 10400 6400 50  0001 C CNN
F 1 "+5V" H 10400 6690 50  0000 C CNN
F 2 "" H 10400 6550 50  0000 C CNN
F 3 "" H 10400 6550 50  0000 C CNN
	1    10400 6550
	-1   0    0    -1  
$EndComp
$Comp
L power:VDD #PWR019
U 1 1 5EF3558F
P 9350 6400
F 0 "#PWR019" H 9350 6250 50  0001 C CNN
F 1 "VDD" H 9350 6550 50  0000 C CNN
F 2 "" H 9350 6400 50  0000 C CNN
F 3 "" H 9350 6400 50  0000 C CNN
	1    9350 6400
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR020
U 1 1 5EF35978
P 8400 7700
F 0 "#PWR020" H 8400 7550 50  0001 C CNN
F 1 "VSS" H 8400 7850 50  0000 C CNN
F 2 "" H 8400 7700 50  0000 C CNN
F 3 "" H 8400 7700 50  0000 C CNN
	1    8400 7700
	-1   0    0    1   
$EndComp
$Comp
L mysensors-gate-rescue:C C5
U 1 1 5EF35BBB
P 8400 7350
F 0 "C5" H 8450 7450 50  0000 L CNN
F 1 "100nF" H 8300 7250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 8438 7200 50  0001 C CNN
F 3 "" H 8400 7350 50  0000 C CNN
	1    8400 7350
	-1   0    0    1   
$EndComp
$Comp
L power:VDD #PWR021
U 1 1 5EF35E74
P 8400 7100
F 0 "#PWR021" H 8400 6950 50  0001 C CNN
F 1 "VDD" H 8400 7250 50  0000 C CNN
F 2 "" H 8400 7100 50  0000 C CNN
F 3 "" H 8400 7100 50  0000 C CNN
	1    8400 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5EF86CC1
P 10400 8250
F 0 "#PWR022" H 10400 8000 50  0001 C CNN
F 1 "GND" H 10400 8100 50  0000 C CNN
F 2 "" H 10400 8250 50  0000 C CNN
F 3 "" H 10400 8250 50  0000 C CNN
	1    10400 8250
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:CONN_01X02 P3
U 1 1 5EF871FF
P 3900 3450
F 0 "P3" H 3900 3600 50  0000 C CNN
F 1 "LIMIT-SW" V 4000 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 3900 3450 50  0001 C CNN
F 3 "" H 3900 3450 50  0000 C CNN
	1    3900 3450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5EF873AB
P 4200 3600
F 0 "#PWR023" H 4200 3350 50  0001 C CNN
F 1 "GND" H 4200 3450 50  0000 C CNN
F 2 "" H 4200 3600 50  0000 C CNN
F 3 "" H 4200 3600 50  0000 C CNN
	1    4200 3600
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:R R4
U 1 1 5EF874CE
P 4200 3150
F 0 "R4" V 4280 3150 50  0000 C CNN
F 1 "100K" V 4200 3150 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4130 3150 50  0001 C CNN
F 3 "" H 4200 3150 50  0000 C CNN
	1    4200 3150
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR024
U 1 1 5EF87653
P 4200 2900
F 0 "#PWR024" H 4200 2750 50  0001 C CNN
F 1 "+5V" H 4200 3040 50  0000 C CNN
F 2 "" H 4200 2900 50  0000 C CNN
F 3 "" H 4200 2900 50  0000 C CNN
	1    4200 2900
	1    0    0    -1  
$EndComp
$Comp
L mysensors-gate-rescue:CONN_01X12 P2
U 1 1 5EF87B6F
P 10700 7200
F 0 "P2" H 10700 7850 50  0000 C CNN
F 1 "AUX" V 10800 7200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x12" H 10700 7200 50  0001 C CNN
F 3 "" H 10700 7200 50  0000 C CNN
	1    10700 7200
	1    0    0    1   
$EndComp
$Comp
L mysensors-gate-rescue:C C6
U 1 1 5EF89C44
P 7600 7200
F 0 "C6" V 7700 7300 50  0000 L CNN
F 1 "100nF" V 7450 7100 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 7638 7050 50  0001 C CNN
F 3 "" H 7600 7200 50  0000 C CNN
	1    7600 7200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7400 3850 8200 3850
Wire Wire Line
	8400 6200 8400 6100
Connection ~ 8400 5700
Wire Wire Line
	8400 5600 8400 5700
Wire Wire Line
	10450 3550 10550 3550
Connection ~ 10550 3550
Wire Wire Line
	10550 3250 10450 3250
Wire Wire Line
	10550 3250 10550 3550
Connection ~ 4300 4700
Wire Wire Line
	4300 4700 4450 4700
Wire Wire Line
	4300 5200 4300 5100
Wire Wire Line
	4300 4100 4300 4200
Wire Wire Line
	4300 4600 4300 4700
Wire Wire Line
	8800 6400 8800 6300
Wire Wire Line
	8800 5800 8800 5900
Wire Wire Line
	5100 3850 5350 3850
Wire Wire Line
	7400 4000 8150 4000
Connection ~ 5100 2950
Wire Wire Line
	5000 2950 5100 2950
Wire Wire Line
	5250 3250 5350 3250
Wire Wire Line
	5250 2700 5250 2800
Wire Wire Line
	7500 3400 7400 3400
Wire Wire Line
	7400 3550 9850 3550
Wire Wire Line
	5200 4700 5200 3550
Wire Wire Line
	5200 3550 5350 3550
Wire Wire Line
	4100 4600 4300 4600
Wire Wire Line
	4300 4500 4100 4500
Wire Wire Line
	5100 2950 5100 3050
Wire Wire Line
	5250 2800 4600 2800
Wire Wire Line
	4600 2800 4600 2950
Wire Wire Line
	4600 2950 4700 2950
Connection ~ 5250 2800
Wire Wire Line
	6100 5700 6600 5700
Wire Wire Line
	6450 4850 6450 5600
Wire Wire Line
	6450 5800 6700 5800
Wire Wire Line
	6350 5450 6350 5900
Wire Wire Line
	6100 5900 6350 5900
Wire Wire Line
	5150 5100 8250 5100
Wire Wire Line
	5150 5100 5150 3700
Wire Wire Line
	5150 3700 5350 3700
Wire Wire Line
	7350 7200 7350 7400
Wire Wire Line
	7350 8600 7350 8500
Wire Wire Line
	5350 4000 4700 4000
Wire Wire Line
	4700 4000 4700 7950
Wire Wire Line
	4700 7950 6900 7950
Wire Wire Line
	6900 8050 4650 8050
Wire Wire Line
	4650 8050 4650 4150
Wire Wire Line
	4650 4150 5350 4150
Wire Wire Line
	5350 4300 4600 4300
Wire Wire Line
	4600 4300 4600 6400
Wire Wire Line
	4600 6400 8050 6400
Wire Wire Line
	8050 6400 8050 7950
Wire Wire Line
	8050 7950 7800 7950
Wire Wire Line
	7800 7850 8000 7850
Wire Wire Line
	6500 7200 6600 7200
Wire Wire Line
	6100 7500 6100 7700
Wire Wire Line
	5600 7100 5600 7200
Wire Wire Line
	5600 7200 5700 7200
Connection ~ 5600 7200
Wire Wire Line
	6600 7100 6600 7200
Connection ~ 6600 7200
Wire Wire Line
	5600 7600 5600 7700
Wire Wire Line
	5600 7700 6100 7700
Connection ~ 6100 7700
Wire Wire Line
	6600 7700 6600 7600
Wire Wire Line
	7950 5000 7550 5000
Wire Wire Line
	7550 5000 7550 4150
Wire Wire Line
	7550 4150 7400 4150
Wire Wire Line
	8000 4300 7400 4300
Wire Wire Line
	8150 5000 10050 5000
Wire Wire Line
	10050 4350 10050 5000
Wire Wire Line
	8100 4600 9450 4600
Connection ~ 10050 5000
Wire Wire Line
	8250 4800 9750 4800
Wire Wire Line
	9450 4600 9450 4350
Connection ~ 9450 4600
Wire Wire Line
	9750 4350 9750 4800
Connection ~ 9750 4800
Wire Wire Line
	9900 4350 9900 4900
Connection ~ 9900 4900
Wire Wire Line
	10200 4450 10200 5200
Wire Wire Line
	10250 5300 10500 5300
Wire Wire Line
	10250 5300 10250 5700
Wire Wire Line
	8400 5200 10200 5200
Connection ~ 10200 5200
Wire Wire Line
	10400 5100 10400 4500
Wire Wire Line
	8800 5400 9300 5400
Connection ~ 9300 5400
Wire Wire Line
	9150 4350 9150 5600
Connection ~ 9150 5600
Wire Wire Line
	9300 4350 9300 5400
Wire Wire Line
	8400 5200 8400 5300
Wire Wire Line
	8250 5100 8250 5700
Wire Wire Line
	8250 5700 8400 5700
Wire Wire Line
	8800 5400 8800 5500
Wire Wire Line
	5100 3850 5100 5050
Wire Wire Line
	5100 5050 8650 5050
Wire Wire Line
	8650 5050 8650 5900
Wire Wire Line
	8650 5900 8800 5900
Connection ~ 8800 5900
Wire Wire Line
	8200 3850 8200 4500
Wire Wire Line
	8150 4000 8150 5000
Wire Wire Line
	8250 4800 8250 3700
Wire Wire Line
	8250 3700 7400 3700
Wire Wire Line
	8100 4600 8100 4450
Wire Wire Line
	8100 4450 7400 4450
Wire Wire Line
	10400 5100 10500 5100
Wire Wire Line
	6300 4850 6300 5150
Wire Wire Line
	6300 5150 9050 5150
Wire Wire Line
	9050 5150 9050 5600
Wire Wire Line
	6600 4850 6600 5700
Wire Wire Line
	6250 5800 6250 6000
Wire Wire Line
	6250 6000 6700 6000
Wire Wire Line
	7950 5000 7950 6350
Wire Wire Line
	7950 6350 6800 6350
Wire Wire Line
	6800 6350 6800 7850
Wire Wire Line
	6800 7850 6900 7850
Wire Wire Line
	7500 3400 7500 3250
Wire Wire Line
	7500 3250 9850 3250
Wire Wire Line
	5250 4900 9900 4900
Wire Wire Line
	5250 4900 5250 4450
Wire Wire Line
	5250 4450 5350 4450
Wire Wire Line
	9000 4350 9000 4500
Wire Wire Line
	8200 4500 9000 4500
Connection ~ 9000 4500
Wire Wire Line
	9050 5600 9150 5600
Wire Wire Line
	9000 5500 10500 5500
Wire Wire Line
	9600 4350 9600 4700
Wire Wire Line
	7050 4700 9600 4700
Wire Wire Line
	7050 4700 7050 4950
Wire Wire Line
	7050 4950 6150 4950
Wire Wire Line
	6150 4950 6150 4850
Connection ~ 9600 4700
Connection ~ 6350 5900
Wire Wire Line
	6100 5800 6250 5800
Connection ~ 6250 6000
Connection ~ 6600 5700
Wire Wire Line
	6450 5600 6100 5600
Connection ~ 6450 5600
Wire Wire Line
	4450 4600 4450 4700
Connection ~ 4450 4700
Wire Wire Line
	4450 4300 4450 4200
Wire Wire Line
	4450 4200 4300 4200
Connection ~ 4300 4200
Wire Wire Line
	8000 7850 8000 4300
Wire Wire Line
	9150 6400 9150 6500
Wire Wire Line
	9050 8200 9050 8300
Wire Wire Line
	8150 8300 9050 8300
Wire Wire Line
	8150 8300 8150 6700
Wire Wire Line
	8150 6700 6450 6700
Connection ~ 6450 5800
Wire Wire Line
	6600 6650 8200 6650
Wire Wire Line
	8200 6650 8200 8350
Wire Wire Line
	8200 8350 9150 8350
Wire Wire Line
	9150 8350 9150 8200
Wire Wire Line
	8600 7000 8600 7100
Wire Wire Line
	8600 7000 8700 7000
Wire Wire Line
	8700 7100 8600 7100
Connection ~ 8600 7100
Wire Wire Line
	8700 7200 8600 7200
Connection ~ 8600 7200
Wire Wire Line
	9350 6400 9350 6500
Wire Wire Line
	9350 6500 9150 6500
Connection ~ 9150 6500
Wire Wire Line
	8400 7500 8400 7600
Wire Wire Line
	8400 7600 8600 7600
Connection ~ 8600 7600
Connection ~ 8400 7600
Wire Wire Line
	8400 7100 8400 7200
Wire Wire Line
	4100 3400 4200 3400
Wire Wire Line
	4200 3600 4200 3500
Wire Wire Line
	4200 3500 4100 3500
Wire Wire Line
	4200 3300 4200 3400
Connection ~ 4200 3400
Wire Wire Line
	4200 2900 4200 3000
Wire Wire Line
	9800 7050 10500 7050
Wire Wire Line
	10500 7150 9800 7150
Wire Wire Line
	9800 7250 10500 7250
Wire Wire Line
	10500 7350 9800 7350
Wire Wire Line
	10500 7450 9800 7450
Wire Wire Line
	10500 7550 9800 7550
Wire Wire Line
	10500 7650 9800 7650
Wire Wire Line
	9800 7750 10500 7750
Connection ~ 9150 8350
Connection ~ 9050 8300
Wire Wire Line
	10400 6550 10400 6650
Wire Wire Line
	10400 6650 10500 6650
Wire Wire Line
	10400 8250 10400 6750
Wire Wire Line
	10400 6750 10500 6750
Wire Wire Line
	10050 8300 10050 6850
Wire Wire Line
	10050 6850 10500 6850
Wire Wire Line
	10500 6950 10100 6950
Wire Wire Line
	10100 6950 10100 8350
Connection ~ 7350 7200
$Comp
L power:GND #PWR025
U 1 1 5EF8A306
P 7850 7300
F 0 "#PWR025" H 7850 7050 50  0001 C CNN
F 1 "GND" H 7850 7150 50  0000 C CNN
F 2 "" H 7850 7300 50  0000 C CNN
F 3 "" H 7850 7300 50  0000 C CNN
	1    7850 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 7300 7850 7200
Wire Wire Line
	7850 7200 7750 7200
Wire Wire Line
	8400 5700 8400 5800
Wire Wire Line
	10550 3550 10550 3700
Wire Wire Line
	4300 4700 4300 4800
Wire Wire Line
	5100 2950 5350 2950
Wire Wire Line
	5250 2800 5250 3250
Wire Wire Line
	5600 7200 5600 7300
Wire Wire Line
	6600 7200 7350 7200
Wire Wire Line
	6600 7200 6600 7300
Wire Wire Line
	6100 7700 6100 8150
Wire Wire Line
	6100 7700 6600 7700
Wire Wire Line
	10050 5000 10500 5000
Wire Wire Line
	9450 4600 10500 4600
Wire Wire Line
	9750 4800 10500 4800
Wire Wire Line
	9900 4900 10500 4900
Wire Wire Line
	10200 5200 10500 5200
Wire Wire Line
	9300 5400 10500 5400
Wire Wire Line
	9150 5600 10500 5600
Wire Wire Line
	8800 5900 8800 6000
Wire Wire Line
	9000 4500 9000 5500
Wire Wire Line
	9600 4700 10500 4700
Wire Wire Line
	6350 5900 6700 5900
Wire Wire Line
	6250 6000 6250 6100
Wire Wire Line
	6600 5700 6700 5700
Wire Wire Line
	6600 5700 6600 6650
Wire Wire Line
	6450 5600 6450 5800
Wire Wire Line
	4450 4700 5200 4700
Wire Wire Line
	4300 4200 4300 4500
Wire Wire Line
	6450 5800 6450 6700
Wire Wire Line
	8600 7100 8600 7200
Wire Wire Line
	8600 7200 8600 7600
Wire Wire Line
	9150 6500 9150 6600
Wire Wire Line
	8600 7600 8600 7700
Wire Wire Line
	8400 7600 8400 7700
Wire Wire Line
	4200 3400 5350 3400
Wire Wire Line
	9150 8350 10100 8350
Wire Wire Line
	9050 8300 10050 8300
Wire Wire Line
	7350 7200 7450 7200
$EndSCHEMATC
