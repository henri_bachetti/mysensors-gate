
#define LOG_DOMAIN LOG_TEMP
#include "debug.h"
#include "options.h"
#include "temp.h"

#if SENSOR != NONE

#if SENSOR == HDC2080
#include <HDC2080.h>
#elif SENSOR == SHT31D
#include <Adafruit_SHT31.h>
#else
#include <Adafruit_HTU21DF.h>
#endif

#if SENSOR == HDC2080
HDC2080 hdc2080(0x40);
#elif SENSOR == SHT31D
Adafruit_SHT31 sht31 = Adafruit_SHT31();
#else
Adafruit_HTU21DF htu21 = Adafruit_HTU21DF();
#endif

void getTemperatureHumidity(float *temperature, float *humidity)
{
#if SENSOR == HDC2080
  *temperature = hdc2080.readTemp();
  *humidity = hdc2080.readHumidity();
#elif SENSOR == SHT31D
  *temperature = sht31.readTemperature();
  *humidity = sht31.readHumidity();
#else
  *temperature = htu21.readTemperature();
  *humidity = htu21.readHumidity();
#endif
}

bool tempSensorBegin(void)
{
#if SENSOR == HDC2080
  hdc2080.begin();
  hdc2080.setMeasurementMode(TEMP_AND_HUMID);
  hdc2080.setRate(ONE_HZ);
  hdc2080.setTempRes(FOURTEEN_BIT);
  hdc2080.setHumidRes(FOURTEEN_BIT);
  hdc2080.triggerMeasurement();
#elif SENSOR == SHT31D
  if (!sht31.begin(0x44)) {
    log_println("Couldn't find SHT31");
    return false;
  }
#else
  if (!htu21.begin()) {
    log_println("Couldn't find sensor!");
    return false;
  }
#endif
  return true;
}

#endif
