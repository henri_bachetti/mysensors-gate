
#ifndef _LOWPOWER_H_
#define _LOWPOWER_H_

void lowPowerBegin(void);
int buttonPressed(void);
int lowPowerSleep(int seconds, int *btn);

#endif
