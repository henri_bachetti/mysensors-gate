
#ifndef _SOLAR_H_
#define _SOLAR_H_

#include "options.h"

class SolarPowerSupply
{
  public:
    SolarPowerSupply();
#if OPEN_CLOSE_METHOD == USE_LDR
    float getLuminosity(void);
#endif
    void enableCharger(int state);
    float getPanelVoltage(void);
    float getPanelCurrent(void);
    float getBatteryVoltage(bool waitDelay);
    unsigned int getBatteryCapacity(float *vBatt);
};

#endif
