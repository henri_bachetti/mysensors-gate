
#include <Time.h>

#define LOG_DOMAIN LOG_MAIN
#include "debug.h"
#include "options.h"
#include "lowpower.h"
#include "solar.h"
#include "timedate.h"
#include "shell.h"
#include "temp.h"

#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
#define MY_RADIO_RF24
//#define MY_RADIO_RFM69
#define MY_RF24_CE_PIN    7
#define MY_RF24_CS_PIN    8
//#define MY_DEBUG
#include <SPI.h>
#include <MySensors.h>

#define CHILD_ID_GATE         0
#if SENSOR != NONE
#define CHILD_ID_TEMP         1
#define CHILD_ID_HUM          2
#endif
#define CHILD_ID_VOLTAGE      3
#endif
#ifdef USE_INA138
#define CHILD_ID_IPANEL       4
#endif

#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
boolean timeReceived = false;
#endif

#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
#define SN "MYSENSORS Automated Gate"
#else
#define SN "Automated Gate"
#endif
#define SV "1.0"

#if MOTOR == SERVO_MOTOR
#include "mg996r.h"
#elif MOTOR == DC_MOTOR
#include "l293d.h"
#else
#error "directive MOTOR is undefined or invalid"
#endif

//#define TEST

#if MOTOR == SERVO_MOTOR
#define gateState()     motor.state()
#define powerOffGate()  motor.powerOff()
MG996R motor(SERVO_PIN, VCC2_PIN);
#elif MOTOR == DC_MOTOR
#define gateState()     motor.state()
#define powerOffGate()  motor.set(0, 0)
L293D motor(L293D_EN_PIN, L293D_1_PIN, L293D_2_PIN, VCC1_PIN, VCC2_PIN);
#endif

void flashLed(int n)
{
  for (int i = 0 ; i < n ; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
  }
}

#if OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ
GateScheduler gateScheduler;
#endif

SolarPowerSupply power;
float battVoltage;
int battSamples;

void getBattVoltage(void)
{
  battSamples++;
  battVoltage += power.getBatteryVoltage(false);
}

void openGate(void)
{
  battSamples = 0;
  battVoltage = power.getBatteryVoltage(true);
#if MOTOR == SERVO_MOTOR
  motor.open(getBattVoltage);
#elif MOTOR == DC_MOTOR
  motor.set(PWM, getBattVoltage);
#endif
  battVoltage /= battSamples;
  log_print(F("min voltage ")); log_println(battVoltage);
  if (battVoltage < MIN_BATT_VOLTAGE) {
    log_print(F("battery is LOW\n"));
    flashLed(5);
  }
  int state = gateState();
  log_print(F("gate state: ")); log_println(state == HIGH ? F("HIGH") : F("LOW"));
}

void closeGate(void)
{
  battSamples = 0;
  battVoltage = power.getBatteryVoltage(true);
#if MOTOR == SERVO_MOTOR
  motor.close(getBattVoltage);
#elif MOTOR == DC_MOTOR
  motor.set(-PWM, getBattVoltage);
#endif
  battVoltage /= battSamples;
  log_print(F("min voltage ")); log_println(battVoltage);
  if (battVoltage < MIN_BATT_VOLTAGE) {
    log_print(F("battery is LOW"));
    flashLed(5);
  }
  int state = gateState();
  log_print(F("gate state: ")); log_println(state == HIGH ? F("HIGH") : F("LOW"));
}

void checkLuminosity(void)
{
  log_println(F("checkLuminosity"));
  int state = gateState();
  log_print(F("gate state: ")); log_println(state == HIGH ? F("HIGH") : F("LOW"));
#if OPEN_CLOSE_METHOD == USE_LDR
  float lux = power.getLuminosity();
  log_print(F("LDR: ")); log_println(lux);
  if (lux > LDR_DAY_LEVEL && state != HIGH) {
#elif OPEN_CLOSE_METHOD == USE_PANEL
  float vPanel = power.getPanelVoltage();
  log_print(F("panel: ")); log_println(vPanel);
  if (vPanel > LDR_DAY_LEVEL && state != HIGH) {
#elif OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ
  bool isDay = gateScheduler.isDay();
  log_print(F("day: ")); log_println(isDay ? F("YES") : F("NO"));
  if (isDay && state != HIGH) {
#endif
    log_println(F("### AUTO OPEN"));
    openGate();
  }
#if OPEN_CLOSE_METHOD == USE_LDR
  if (lux < LDR_NIGHT_LEVEL && state != LOW) {
#elif OPEN_CLOSE_METHOD == USE_PANEL
  if (vPanel < LDR_NIGHT_LEVEL && state != LOW) {
#elif OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ
  if (!isDay && state != LOW) {
#endif
    log_println(F("### AUTO CLOSE"));
    closeGate();
  }
  powerOffGate();
#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
  MyMessage msgGate(0, V_STATUS);
  send(msgGate.setSensor(CHILD_ID_GATE).set(gateState()));
#endif
}

void before(void)
{
  log_println(F(SN));
  pinMode(LED_BUILTIN, OUTPUT);
  power.enableCharger(HIGH);
}

#if OPEN_CLOSE_METHOD == USE_DOMOTICZ

void presentation()
{
  // Send the Sketch Version Information to the Gateway
  log_print("Presentation: ");
  present(CHILD_ID_GATE, S_BINARY);
#if SENSOR != NONE
  present(CHILD_ID_TEMP, S_TEMP);
  present(CHILD_ID_HUM, S_HUM);
#endif
  present(CHILD_ID_VOLTAGE, S_MULTIMETER);
#ifdef USE_INA138
  present(CHILD_ID_IPANEL, S_POWER);
#endif
  sendSketchInfo(SN, SV);
  log_println("OK");
}

void receiveTime(unsigned long controllerTime)
{
  log_print("Time value received: ");
  log_println(controllerTime);
  gateScheduler.adjust(controllerTime);
  checkLuminosity();
  timeReceived = true;
}

#if SENSOR != NONE
void sendTemperatureHumidity(void)
{
  float temperature;
  float humidity;

  getTemperatureHumidity(&temperature, &humidity);
  log_print(F("temperature/humidity: ")); log_print(temperature); log_print(F("/")); log_println(humidity);
  MyMessage msgTemp(0, V_TEMP);
  MyMessage msgHum(0, V_HUM);
  send(msgTemp.setSensor(CHILD_ID_TEMP).set(temperature, 1));
  send(msgHum.setSensor(CHILD_ID_HUM).set(humidity, 1));
}
#endif
#endif

void setup()
{
#if OPEN_CLOSE_METHOD != USE_DOMOTICZ
  Serial.begin(115200);
  before();
#else
  requestTime();
#endif
  motor.begin();
  powerOffGate();
  lowPowerBegin();
#if SENSOR != NONE
  tempSensorBegin();
#endif
#if OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ
  gateScheduler.begin();
#endif
#if OPEN_CLOSE_METHOD != USE_DOMOTICZ
  checkLuminosity();
#endif
}

unsigned long upTime;
unsigned long manualStartTime;
unsigned long manualWakeupTime;
unsigned manualElapsed;

unsigned long getSleepTime(void)
{
  // AUTO mode
  if (manualStartTime == 0) {
    return SLEEP_TIME;
  }
  // manual mode
  if (SLEEP_TIME <= MANUAL_TIMEOUT) {
    return min(SLEEP_TIME, MANUAL_TIMEOUT - manualElapsed);
  }
  return MANUAL_TIMEOUT;
}

void executeManualMode(int btn)
{
  int state = gateState();
  if (btn == OPEN_PIN) {
    if (state != HIGH) {
      log_println(F("### MANUAL OPEN"));
      manualWakeupTime = 0;
      manualStartTime = upTime;
      manualElapsed = 0;
      openGate();
    }
  }
  else if (btn == CLOSE_PIN) {
    if (state != LOW) {
      log_println(F("### MANUAL CLOSE"));
      manualWakeupTime = 0;
      manualStartTime = upTime;
      manualElapsed = 0;
      closeGate();
    }
  }
  else if (btn == OPEN_AND_CLOSE) {
    log_println(F("### enter manual wakeup"));
    manualWakeupTime = millis();
  }
  log_println(F("STOP"));
  powerOffGate();
}

void loop()
{
  static unsigned long lastTimeUpdate = 0, lastRequest = 0;
  unsigned sleepTime;
  float vBatt;
  int btn = 0;

#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
  // Update time
  if ((!timeReceived) || (timeReceived && upTime - lastRequest > TIME_REQUEST_PERIOD)) {
    if (!timeReceived) {
      log_println(F("time not received yet"));
    }
    requestTime();
    lastRequest = upTime;
    log_println(F("wait for time"));
    delay(1000);
    return;
  }
  float vPanel = power.getPanelVoltage();
  log_print(F("panel V: ")); log_println(vPanel);
#ifdef USE_INA138
  MyMessage ampMsg(CHILD_ID_IPANEL, V_WATT);
  float iPanel = power.getPanelCurrent();
  log_print(F("panel I: ")); log_println(iPanel);
  send(ampMsg.setSensor(CHILD_ID_IPANEL).set(vPanel * iPanel, 2));
#endif
  int battLevel = power.getBatteryCapacity(&vBatt);
  log_print(F("battery: ")); log_print(vBatt); log_print("V ("); log_print(battLevel); log_println("%)");
#if OPEN_CLOSE_METHOD == USE_DOMOTICZ && SENSOR != NONE
  sendTemperatureHumidity();
#endif
  MyMessage voltageMsg(CHILD_ID_VOLTAGE, V_VOLTAGE);
  send(voltageMsg.setSensor(CHILD_ID_VOLTAGE).set(vPanel, 2));
  sendBatteryLevel(battLevel);
#endif
  int elapsed = lowPowerSleep(getSleepTime(), &btn);
  upTime += elapsed;
  if (btn != 0) {
    executeManualMode(btn);
  }
  log_print(F("uptime: ")); log_println(upTime);
  log_print(F("manualStartTime: ")); log_println(manualStartTime);
  log_print(F("manualWakeupTime: ")); log_println(manualWakeupTime);
#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
  MyMessage msgGate(0, V_STATUS);
  send(msgGate.setSensor(CHILD_ID_GATE).set(gateState()));
#endif
  if (manualStartTime) {
    manualElapsed += elapsed;
    log_print(F("manual mode, remain: ")); log_println(MANUAL_TIMEOUT - manualElapsed);
    if (manualElapsed >= MANUAL_TIMEOUT) {
      log_println(F("reset manual mode"));
      manualStartTime = 0;
    }
  }
  if (manualStartTime == 0 && manualWakeupTime == 0) {
#if OPEN_CLOSE_METHOD == USE_DOMOTICZ
    if (timeReceived) {
      checkLuminosity();
    }
#else
    checkLuminosity();
#endif
  }
#if OPEN_CLOSE_METHOD == USE_RTC || OPEN_CLOSE_METHOD == USE_DOMOTICZ
  if (manualWakeupTime) {
    waitForShellCommand();
  }
#endif
}
